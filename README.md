# MetaCentrum OpenStack Terraform examples

MetaCentrum OpenStack cloud Terraform use-case examples.

## How to generate infrastructure

1. Install terraform client following [official guide](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli#install-terraform).
1. Clone this repository
1. Load you OpenStack application credentials to environment variables `source ~/conf/prod-meta-cloud-new-openstack-all-roles`, see more details [here](https://docs.cloud.muni.cz/cloud/cli/#getting-credentials).
1. Override any variable if needed in a [main.tf](linux-vm-instances/main.tf). Every variable specified in [common/modules/vm-instances/variables.tf](common/modules/vm-instances/variables.tf)
1. Go to the use-case directory (for example `cd linux-vm-instances/`)
1. Initiate terraform `terraform init`.
1. Validate terraform description `terraform validate`.
1. Plan terraform infrastructure deploy `terraform plan -out plan` (generates terraform plan named `plan`).
1. Deploy infrastructure with `terraform apply plan` with previously generated plan.

## How to destroy infrastructure

1. Create plan to destroy infrastructure `terraform plan -destroy -out destroy-plan`
1. Destroy infrastructure `terraform apply destroy-plan`

## Use cases

### Create Linux VM server instances, each with FIP address [`linux-vm-instances`](linux-vm-instances)

 * create [`instances_count`](common/modules/vm-instances/variables.tf) Linux VM instances
   * you may costomize VM size (`instances_ostack_flavor`) and VM operating system (`instances_ostack_image`)
 * create internal dedicated network for them ([`instances_ostack_network_cidr`](common/modules/vm-instances/variables.tf))
 * each VM instance may have additional OpenStack volume ([`instances_volume_(size|count)`](common/modules/vm-instances/variables.tf))
 * each VM gets FIP (floating IP address) from pool ([`instances_ostack_fip_pool`](common/modules/vm-instances/variables.tf))
 * security groups are assigned to each VM ([`secgroup_rules.tf`](common/modules/vm-instances/secgroup_rules.tf))


### Create Windows VM server instances, each with FIP address [`windows-vm-instances`](windows-vm-instances)

Similar as above.
