terraform {
  backend "local" {}
}

terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.47.0"
    }
  }
}


provider "openstack" {
  # all values are loaded from env. variables openstack RC file, see REAME.md
}


module "linux-vm-instances" {
  source                        = "../common/modules/vm-instances"

  # Example of variable overrides
  instances_count               = 3
  environment                   = "linux-vm-farm"
  public_key                    = "~/.ssh/id_rsa.pub"

  instances_ostack_fip_pool     = "private-muni-10-16-116"
  instances_ostack_network_cidr = "10.250.0.0/24"
  instances_ostack_flavor       = "hpc.8core-128ram"
  instances_ostack_image        = "88f8e72a-bbf0-4ccc-8ff2-4f3188cd0d18"

  # additional volumes
  instances_volumes_count       = 3     # place volumes
  instances_volumes_size        = 100   # each 100 GB
}
