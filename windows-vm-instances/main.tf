terraform {
  backend "local" {}
}

terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.47.0"
    }
  }
}


provider "openstack" {
  # all values are loaded from env. variables openstack RC file, see REAME.md
}


module "windows-vm-instances" {
  source                        = "../common/modules/vm-instances"

  # Example of variable overrides
  instances_count               = 3
  environment                   = "win-vm-farm"

  instances_ostack_fip_pool     = "private-muni-10-16-116"
  instances_ostack_network_cidr = "10.250.0.0/24"
  instances_ostack_flavor       = "hpc.8core-128ram"
  instances_ostack_image        = "97c8b617-2f82-4ba7-9e4a-4bb5e289acbb" # windows-server-2019-standard-eval-x86_64

  # additional volumes
  instances_volumes_count       = 3 # place volumes
  instances_volumes_size        = 100   # each 100 GB
}
