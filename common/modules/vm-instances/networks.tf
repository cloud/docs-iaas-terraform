###############################################################
# Define networking                                           #
# Security group rules are in separate file secgroup_rules.tf #
###############################################################

resource "openstack_networking_network_v2" "instances_network_default" {
  name           = "${var.environment}_network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "instances_subnet_default" {
  name            = "${var.environment}_subnet"
  network_id      = openstack_networking_network_v2.instances_network_default.id
  cidr            = var.instances_ostack_network_cidr
  ip_version      = 4
  dns_nameservers = ["1.1.1.1", "8.8.8.8"]
}

data "openstack_networking_network_v2" "instances_external_network" {
  name = var.instances_ostack_fip_pool
}

resource "openstack_networking_router_v2" "instances_router_default" {
  name                = "${var.environment}_router"
  admin_state_up      = "true"
  external_network_id = data.openstack_networking_network_v2.instances_external_network.id
}

resource "openstack_networking_router_interface_v2" "instances_router_interface" {
  router_id = openstack_networking_router_v2.instances_router_default.id
  subnet_id = openstack_networking_subnet_v2.instances_subnet_default.id
}


# Floating IPs
resource "openstack_networking_floatingip_v2" "fip_addresses" {
  count              = var.instances_count
  pool               = var.instances_ostack_fip_pool
}

resource "openstack_networking_floatingip_associate_v2" "fip_to_port_associations" {
  count       = var.instances_count
  floating_ip = openstack_networking_floatingip_v2.fip_addresses[count.index].address
  port_id     = openstack_networking_port_v2.instances_ports[count.index].id
}


resource "openstack_networking_port_v2" "instances_ports" {
  count              = var.instances_count
  name               = "${var.environment}_port_${count.index+1}"
  network_id         = openstack_networking_network_v2.instances_network_default.id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.instances_subnet_default.id
  }
}
