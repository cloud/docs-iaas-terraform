
resource "openstack_compute_keypair_v2" "pubkey" {
  name       = "${var.environment}-keypair"
  public_key = file("${var.public_key}")
}

