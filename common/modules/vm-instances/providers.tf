terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.47.0"
    }
  }
}


provider "openstack" {
  auth_url = "https://identity.cloud.muni.cz"

  application_credential_id     = ""
  application_credential_secret = ""
}
