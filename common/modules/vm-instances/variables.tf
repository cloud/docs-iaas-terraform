variable "environment" {
  description = "Name prefix for all resources. Use a-z, 0-9 and the hyphen (-) only."
  default     = "training"
}

variable "public_key" {
  default = "~/.ssh/id_rsa.pub"
}

# user names
variable "ssh_user_name" {
  default = "ubuntu"
}
variable "rdp_user_name" {
  default = "Admin"
}


##################
# nodes settings #
##################
variable "instances_count" {
  default = 3
}

variable "instances_ostack_flavor" {
  description = "OpenStack flavor used for instances"
  default = "hpc.8core-32ram-ssd-ephem"
}

variable "instances_ostack_image" {
  description = "OpenStack image used for instances"
  default = "88f8e72a-bbf0-4ccc-8ff2-4f3188cd0d18"
}

variable "instances_ostack_network_cidr" {
  description = "Internal OpenStack network address, use CIDR notation"
  default     = "10.0.0.0/24"
}

variable "instances_ostack_fip_pool" {
  description = "FIP pool"
  default     = "public-cesnet-195-113-167-GROUP"
}


variable "instances_volumes_count" {
  description = "Number of volumes to be attached to instances (0 to disable attaching volumes, 1 attach to first instance only, ...)"
  default     = ""
}

variable "instances_volumes_size" {
  description = "Size of volume attached to instances (in GB)"
  default     = 500
}
