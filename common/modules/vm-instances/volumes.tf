
resource "openstack_blockstorage_volume_v3" "instances_volumes" {
  count = var.instances_volumes_count != "" ? var.instances_volumes_count : var.instances_count
  name  = "${var.environment}-volume-${count.index+1}"
  size  = var.instances_volumes_size
}

resource "openstack_compute_volume_attach_v2" "instances_volume_attachments" {
  count = var.instances_volumes_count != "" ? var.instances_volumes_count : var.instances_count
  instance_id = element(openstack_compute_instance_v2.instances.*.id, count.index)
  volume_id   = element(openstack_blockstorage_volume_v3.instances_volumes.*.id, count.index)
  device = "/dev/sdb"
}

