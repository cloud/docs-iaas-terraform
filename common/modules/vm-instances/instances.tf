
####################
# Define instances #
####################

resource "openstack_compute_instance_v2" "instances" {
  count           = var.instances_count
  name            = "${var.environment}-${count.index+1}"
  image_id        = var.instances_ostack_image
  flavor_name     = var.instances_ostack_flavor
  key_pair        = openstack_compute_keypair_v2.pubkey.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]
  user_data       = "#cloud-config\nhostname: ${var.environment}-${count.index+1}.local\n${file("./cloudinit.txt")}"

  network {
    uuid = openstack_networking_network_v2.instances_network_default.id
    port = element(openstack_networking_port_v2.instances_ports.*.id, count.index)
  }

}

